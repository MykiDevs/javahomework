package javahomework;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class homework2 {
    public static void main(String[] args) {
        String[][] field = {
                { "0|", "1|", "2|", "3|", "4|", "5|" },
                { "1|", "-", "-", "-", "-" },
                { "2|", "-", "-", "-", "-" },
                { "3|", "-", "-", "-", "-" },
                { "4|", "-", "-", "-", "-" },
                { "5|", "-", "-", "-", "-" }
        }; // это поле
        Random random = new Random();
        int targetX = 1 + random.nextInt(4);
        int targetY = 1 + random.nextInt(4);
        field[targetX][targetY] = "x";
        boolean hit = false;

        while (true) {
            System.out.println("All Set. Get ready to rumble!");
            System.out.println("X: ");
            Scanner sc = new Scanner(System.in);
            int x = sc.nextInt();
            sc.nextLine();
            System.out.println("Y: ");
            int y = sc.nextInt();
            if (x < 0 || x > 5 || y < 0 || y > 5) {
                System.out.println("Error");
                continue;
            }
            if (x == 0) {
                x++;
            } else if (y == 0) {
                y++;
            }
            if (x == 5) {
                x--;
            } else if (y == 5) {
                y--;
            }
            field[x][y] = "*";
            if (x == targetX && y == targetY) {
                hit = true;
                System.out.println("You have won!");
                System.exit(0);
            }
            else {
                System.out.println("Miss!");
                System.out.println("Lose try again");
                System.out.println(Arrays.deepToString(field));

            }
            }
        }
    }